package picviewer.src;

import java.io.*;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.*;
import java.awt.event.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.filechooser.FileFilter;

public class PictureViewer {
    // 变量
    private JList<String> fileList = new JList<String>(new DefaultListModel<String>());
    private File currentDir;
    private JLabel imageLabel = new JLabel();
    private final int DEFAULT_WIDTH = 800;
    private final int DEFAULT_HEIGHT = 450;
    private Preferences root = Preferences.userRoot();
    private Preferences node = root.node("/picviewer/src");

    public static void main(String[] args) {
        PictureViewer picViewer = new PictureViewer();
        EventQueue.invokeLater(() -> {
            picViewer.InitUI();
        });
    }

    /**
     * 初始化UI
     */
    public void InitUI() {
        JFrame pvFrame = new BaseFrame(null);

        // 文件列表
        JPanel fileListPanel = new JPanel(null);
        fileListPanel.setBounds(50, 50, 200, 450);
        JScrollPane scrollPane = new JScrollPane(null);
        scrollPane.setBounds(0, 0, 200, 450);
        scrollPane.setFocusTraversalKeysEnabled(true);
        scrollPane.setPreferredSize(new Dimension(200, 100));
        fileList.addListSelectionListener(new ItemClickListener());
        scrollPane.setViewportView(fileList);
        fileListPanel.add(scrollPane);
        fileListPanel.setVisible(true);

        // 打开文件或文件夹
        JPanel fileButtonPanel = new JPanel(null);
        fileButtonPanel.setBounds(50, 500, 200, 40);
        JButton dirButton = new BaseButton("打开文件夹");
        dirButton.setBounds(0, 5, 95, 30);
        dirButton.setFont(new Font("微软雅黑", Font.PLAIN, 10));
        dirButton.addActionListener(new DirClickListener());
        JButton fileButton = new BaseButton("打开文件");
        fileButton.setBounds(105, 5, 95, 30);
        fileButton.setFont(new Font("微软雅黑", Font.PLAIN, 10));
        fileButton.addActionListener(new FileClickListener());
        fileButtonPanel.add(dirButton);
        fileButtonPanel.add(fileButton);
        fileButtonPanel.setVisible(true);

        // 显示图片
        JPanel picturePanel = new JPanel(null);
        picturePanel.setBackground(new ColorUIResource(220, 220, 220));
        picturePanel.setBounds(300, 50, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        imageLabel.setBounds(0, 0, 800, 450);
        picturePanel.add(imageLabel);
        picturePanel.setVisible(true);

        // 切换图片
        JPanel changeButtonPanel = new JPanel(null);
        changeButtonPanel.setBounds(300, 500, 800, 40);
        JButton leftButton = new BaseButton("上一张");
        leftButton.setBounds(0, 5, 100, 30);
        leftButton.setFont(new Font("微软雅黑", Font.PLAIN, 10));
        leftButton.addActionListener(new ChangePictureListener(-1));
        JButton rightButton = new BaseButton("下一张");
        rightButton.setBounds(700, 5, 100, 30);
        rightButton.setFont(new Font("微软雅黑", Font.PLAIN, 10));
        rightButton.addActionListener(new ChangePictureListener(1));
        changeButtonPanel.add(leftButton);
        changeButtonPanel.add(rightButton);
        changeButtonPanel.setVisible(true);

        // 向Frame添加组件
        pvFrame.add(fileListPanel);
        pvFrame.add(fileButtonPanel);
        pvFrame.add(picturePanel);
        pvFrame.add(changeButtonPanel);

        pvFrame.setVisible(true);
    }

    // 切换图片函数
    private void changePicture() {
        String filename = fileList.getSelectedValue();
        String filepath = currentDir.toString() + "/" + filename;
        ImageIcon img = new ImageIcon(filepath);
        int width = img.getIconWidth();
        int height = img.getIconHeight();
        if (height / DEFAULT_HEIGHT > width / DEFAULT_WIDTH) {
            width = width * DEFAULT_HEIGHT / height;
            height = DEFAULT_HEIGHT;
        } else {
            height = height * DEFAULT_WIDTH / width;
            width = DEFAULT_WIDTH;
        }
        imageLabel.setBounds((DEFAULT_WIDTH - width) / 2, (DEFAULT_HEIGHT - height) / 2, width, height);
        Image newimg = img.getImage().getScaledInstance(width, height, 0);
        img.setImage(newimg);
        imageLabel.setIcon(img);
    }

    /**
     * 事件监听器
     */
    // 选择文件事件监听器
    class FileClickListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            // 文件选择器的一些设置
            fileChooser.setCurrentDirectory(new File(node.get("currentDir", ".")));
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setMultiSelectionEnabled(true);
            fileChooser.setFileFilter(new PicFileFilter());

            fileChooser.showOpenDialog((Component) e.getSource());
            File[] chosenFile = fileChooser.getSelectedFiles();
            currentDir = fileChooser.getCurrentDirectory();
            node.put("currentDir", currentDir.getPath());
            DefaultListModel<String> fileListModel = (DefaultListModel<String>) fileList.getModel();
            for (File file : chosenFile)
                fileListModel.addElement(file.getName());
            fileList.setModel(fileListModel);
        }
    }

    // 选择文件夹事件监听器
    class DirClickListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            // 文件选择器的一些设置
            fileChooser.setCurrentDirectory(new File(node.get("currentDir", ".")));
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            fileChooser.showOpenDialog((Component) e.getSource());

            currentDir = fileChooser.getSelectedFile();
            node.put("currentDir", currentDir.getPath());
            DefaultListModel<String> fileListModel = (DefaultListModel<String>) fileList.getModel();
            fileListModel.clear();
            for (File file : currentDir.listFiles(new PicFileFilter()))
                fileListModel.addElement(file.getName());
            fileList.setModel(fileListModel);
        }
    }

    // 列表项选择事件监听器
    class ItemClickListener implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {

            changePicture();
        }
    }

    class ChangePictureListener implements ActionListener {
        private int dir = 1;

        public ChangePictureListener(int dir) {
            this.dir = dir;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int index = fileList.getSelectedIndex();
            int number = fileList.getModel().getSize();
            index += dir;
            if (index < 0) {
                index = number - 1;
            }
            if (index >= number) {
                index = 0;
            }
            fileList.setSelectedIndex(index);
            changePicture();
        }

    }

    /**
     * 其他内部类
     */
    // 文件过滤器
    class PicFileFilter extends FileFilter implements FilenameFilter {

        private boolean judgename(String name) {
            Pattern filePattern = Pattern.compile("jpg|png|gif$");
            Matcher fileMatcher = filePattern.matcher(name);
            if (fileMatcher.find())
                return true;
            else
                return false;
        }

        @Override
        public boolean accept(File f) {
            String name = f.getName();
            if (judgename(name) || f.isDirectory())
                return true;
            else
                return false;
        }

        @Override
        public boolean accept(File dir, String name) {
            if (judgename(name))
                return true;
            else
                return false;
        }

        @Override
        public String getDescription() {

            return new String("JPG and GIF Images");
        }
    }
}
