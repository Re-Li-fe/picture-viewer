package picviewer.src;

import javax.swing.*;

public class BaseButton extends JButton {
    public BaseButton(String text, Action a) {
        super(text);
        setAction(a);
    }

    public BaseButton() {
    }

    public BaseButton(String text) {
        super(text);
    }
}
