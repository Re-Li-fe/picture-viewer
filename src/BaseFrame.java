package picviewer.src;

import javax.swing.*;
import java.awt.*;

public class BaseFrame extends JFrame {
    public BaseFrame(LayoutManager layout) {
        super();

        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenWidth = (int) screenSize.getWidth();
        int screenHeight = (int) screenSize.getHeight();
        setBounds(screenWidth / 8, screenHeight / 8, screenWidth / 4 * 3, screenHeight / 4 * 3);

        setLayout(layout);

        setTitle("图片查看器");
    }
}
